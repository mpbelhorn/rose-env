ROSE Compiler Dependency Software Environment
=============================================


To Get Started
--------------

Simply clone this repository to where you would like to install your software
environment and run

```
./setup.sh
```

Please review `./setup.sh` to see what is being done in detail.

It will create a [spack virtual environment](https://spack.readthedocs.io/en/latest/tutorial_environments.html)
named "rose" that refers to `/sw/summit/.swci/0-core` software as an upstream of
a [spack chain](https://spack.readthedocs.io/en/latest/chain.html).

The initial configuration for the environment is captured in the
`rose.spack.yaml` environment configuration file. Some of this configuration is
duplicated in the bare spack instance from files contained in
`./etc/spack/*.yaml`, though it is preferable to use the software installed via
the "spack environment". This initial "spack environment configuration" can be
modified to eliminate the "chain" and build all the dependencies directly, if
that's desired.

The `setup.sh` script creates the "rose" environment with a view at `./opt`. You
should find a directory structure populated with symlinks to the spack-installed
packages of the "rose" environment. Simply add these directories to your
environment *PATH's to use. Or use the `spack env loads` command to use
spack-generated modulefiles for each of the dependency packages.

If you'd rather use modules than a symlink "view", please update `./setup.sh` to
remove the argument that creates the view. Or check with the spack
documentation on how to modify the "rose" environment and views.
