#!/bin/bash

set -e

printf "==> Normalizing the build environment\n"
module reset
module unload DefApps
module load gcc/4.8.5

# Don't let python write *.py[co] files among the spack instance so that any
# changes we make are always honored.
export PYTHONDONTWRITEBYTECODE=1

printf "\n%s\n" "==> Currently loaded modules" "$(module --redirect -t list)"

if [ ! -d spack ]; then
  printf "\n==> Setting up spack\n"
  module load git
  set -x
  git clone https://github.com/spack/spack.git
  cd spack
  # We need to patch some packages and those patches assume we are starting from
  # this commit.
  git checkout -b rose ab21b3d19492e2233c011a120f2414eea989dee0
  # Our spack config.yaml doesn't use $HOME/.spack to store misc. cache data.
  mkdir -p var/spack/misc_cache
  # We need to configure spack to use a repo-specific misc cache and to get some
  # of the packages from the system rather than build them outright.
  cp --update ../etc/spack/* etc/spack/.
  # The automake config scripts shipped with py-gobject are out of date for
  # ppc64le.
  patch --strip 1 <../rose_packages.patch
  cd ..
  set +x
  module unload git
fi

printf "\n==> Validating spack instance\n"
. ./spack/share/spack/setup-env.sh
spack compilers
spack arch

printf "\n==> Setting up the software environment\n"
set +e
spack env list | grep -q "rose"
env_exists="$?"
set -e
if [ ${env_exists} -ne 0 ]; then
  spack env create --with-view $PWD/opt rose rose.spack.yaml
fi

printf "\n==> Installing software\n"
spack env activate rose
spack concretize --force
spack install -v
